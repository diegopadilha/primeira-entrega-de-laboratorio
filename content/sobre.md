---
title: "Sobre"
date: 2022-10-05T18:32:39-03:00
draft: false
---

Tenho muitos _pets_, ao todo são 6: Mila, Tom, Sábio, Petit, Félix e Buck. Todos são gatos, exceto o último, Buck, que é um cachorro.

Minha irmã é veterinária então todos recebem atendimento médico especializado!

